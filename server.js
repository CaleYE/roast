'use strict'

const express = require('express')
const session = require('express-session')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const path = require('path')
const connection = require('./database/connection')
let app = express();

app.use(cookieParser())
app.use(bodyParser.urlencoded( {
	extended: true
}))

app.use(session( {
	resave: true,
	saveUninitialized: true,
	secret: 'hoi Thijs, dit is een geheim',
	cookie: {
		expires:null
	}
}))

app.use(bodyParser.json())

app.set('view engine', 'ejs')

app.use(express.static(path.join(__dirname, 'public')))

connection.connect((err) => {
	if (!err)
		console.log('Connected to database.... OK!')
	else
		console.log('Could not connect to database... Please make sure MySQL service is running and the database is cerated.')
})

let routesindex = require('./routes/index')
let postsindex = require('./routes/posts')
let profileindex = require('./routes/profile')
let timelineindex = require('./routes/timeline')

let indexroutes = [routesindex, postsindex, profileindex, timelineindex]

app.use('/', indexroutes)

app.listen(9191)

console.log('Server is runing at port 9191')