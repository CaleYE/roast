'use strict'

const express = require('express')
const session = require('express-session')
const connection = require('../database/connection')
const crypt = require('../security/encryptor')
let index = express.Router()

index.get('/', (req,res) => {
	var sess = req.session;
	console.log('Get to /')
	res.render('pages/index', {
		loggedin: sess.loggedIn
	})
})

//Login post
index.post('/loginpost', (req,res) => {
	console.log('Post to /loginpost')
	var sess = req.session;

	let PostVariables = { email: req.body.email, password: req.body.password } //Front end fields are named email and password.

	console.log(PostVariables.password)

	let EncryptedPassword = crypt.encrypt(PostVariables.password)

	console.log(EncryptedPassword)

	connection.query("SELECT * FROM users WHERE email = ? AND password = ?",
		[PostVariables.email, EncryptedPassword],
		(error, result) =>{
			if (error) {
				console.log('User tried to login', PostVariables.email, EncryptedPassword)
				res.redirect('/')
			} else {
				if(result.length > 0) {	
					console.log('User logged in:', PostVariables.email, EncryptedPassword)
					sess.loggedIn = true;
					sess.userId = result[0].id;
					res.redirect('/timeline');
				} else {
					res.send('ERROR LOL')
				}
			}
		})
})

/*   Register   */

index.post('/register', (req,res) => {
	console.log('Post to /register')
	var sess = req.session

	let PostVariables = { email: req.body.email, password: req.body.password, displayName: req.body.displayName } //front end fields are named email, password and displayName

	console.log(PostVariables.password)

	let EncryptedPassword = crypt.encrypt(PostVariables.password)

	console.log(EncryptedPassword)

	connection.query("INSERT INTO users (email, password, displayname) VALUES (?,?,?)",
		[PostVariables.email, EncryptedPassword, PostVariables.displayName],
		(error, result) => {
			if (error) {
				console.log('Someone registered with non unique email', error)
				res.redirect('/')
			} else {
				console.log('New user registered:', PostVariables.email, PostVariables.displayName)
				res.redirect('/')
			}
		})
})

module.exports = index