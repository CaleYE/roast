'use strict'

const express = require('express')
const session = require('express-session')
const connection = require('../database/connection')
const crypt = require('../security/encryptor')
let timeline = express.Router()

timeline.get('/timeline', (req,res) => {
	var sess = req.session;
	console.log('Get to /timeline')
	res.render('pages/timeline',{
		loggedin: sess.loggedIn
	})
})

module.exports = timeline