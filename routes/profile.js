'use strict'

const express = require('express')
const session = require('express-session')
const connection = require('../database/connection')
const crypt = require('../security/encryptor')
let profile = express.Router()

/*  Get profile  */
profile.get('/profile/me', (req,res) => {
	console.log('Get to pages/profile')

	 var sess = req.session

	if(sess.loggedIn) {
			connection.query('SELECT * FROM users WHERE id = "' + sess.userId + '"',
			(err,result) => {
				if (!result.length) {
					res.send('ERROR LOL')
					console.log('fucking idiot')
				} else {
					res.render('pages/profile', {
						UserEmail: (result[0].email),
						Name: (result[0].displayname),
						loggedin: sess.loggedIn
					})
				}
			})
	} else {
		res.redirect('/')
	}
})
/*  Post from profile  */
profile.post('/changeprofile', (req,res) => {
	console.log('Post to /changeprofile')

	var sess = req.session

	let ChangeProfilePostVariables = { email: req.body.email, password: req.body.password, displayName: req.body.displayName } //front end input type names are email, password and displayName

	let EncryptedPassword = crypt.encrypt(ChangeProfilePostVariables.password)

	connection.query("UPDATE QUERY TODO",
	[ChangeProfilePostVariables.email, EncryptedPassword, ChangeProfilePostVariables.displayName],
	(error, result) => {
		if (error) {
			console.log('Something went wrong')
		} else {
			console.log('Profile edited')
		}
	})
})

/* get profiles */
profile.get('/profile/:id', (req, res) => {
	var sess = req.session
	console.log('GET to /profile/' + req.params.id)

	connection.query('SELECT * FROM users WHERE id = ?', [req.params.id], (err,result) => {
		if (!result.length) {
			res.send('ERROR LOL')
			console.log('fucking idiot')
		} else {
			var profileId = (result[0].id).toString()
			console.log(profileId)
			res.render('pages/profile', {
				UserEmail: (result[0].email),
				Name: (result[0].displayname),
				loggedin: sess.loggedIn
			})
		}
	})
})

/* Logout */
profile.get('/logout', (req, res) => {
	var sess = req.session
	console.log('GET to /logout')

	sess.destroy((err) => {
		if(err) {
			console.log('Error logging out.')
		} else {
			console.log('Log out?')
			res.redirect('/')
		}
	})
})

module.exports = profile
