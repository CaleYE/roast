'use strict'

const express = require('express')
const session = require('express-session')
const connection = require('../database/connection')
const crypt = require('../security/encryptor')
let posts = express.Router()

/*  post gets  */
posts.get('timeline/roasts', (req, res) => {
	var sess = req.session;
	console.log('GET to /timeline/roasts')

	connection.query("SELECT * FROM posts WHERE postType = '" + "roast'", (err, result) =>{
		if(err)
			console.log('Error. Can\'nt get information from database see error message below: \n\n', err)
		
		res.render('/pages/timeline', {
			PostArray: result, // Use foreach loop in front end to show data.
			loggedin: sess.loggedIn
		})
	})
})

posts.get('/posts/memes', (req, res) => {
	console.log('GET to /timetime/memes')

	connection.query("SELECT * FROM posts WHERE postType = '" + "meme'", (err, result) => {
		if(err)
			console.log('Error Ca\'nt get information from database see error message below: \n\n', err)

		res.render('/pages/timeline', {
			PostArray: result,
			loggedin: sess.loggedIn
		})
	})

})
/*  post POSTs  */
posts.post('/addroast', (req, res) => {
	console.log('POST to /addroast')

	let roast = {title: req.body.roastTitle, body: req.body.roastContent}

	connection.query('INSERT INTO posts (title, desc, postType) VALUES (?,?, "roast")', [roast.title, roast.body], (err, result) =>{
		if(err)
			console.log('Error cant update database see error message below \n\n', err)
		else
			res.redirect('/pages/timeline')
	})
})

posts.post('/addmeme', (req, res) => {
	console.log('POST to /addroast')

	let meme = {title: req.body.memeTitle, body: req.body.memeContent}

	connection.query('INSERT INTO posts (title, desc, postType) VALUES (?,?, "meme")', [meme.title, meme.body], (err, result) =>{
		if(err)
			console.log('Error cant update database see error message below \n\n', err)
		else
			res.redirect('/pages/timeline')
	})
})

module.exports = posts