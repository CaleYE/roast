function updateCountdown() {
    // 140 is the max message length
    var remaining = 255 - jQuery('.message').val().length;
    jQuery('.countdown').text(remaining + ' characters left.');
}

jQuery(document).ready(function($) {
    updateCountdown();
    $('.message').change(updateCountdown);
    $('.message').keyup(updateCountdown);
});